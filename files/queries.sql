/*a. Для заданного списка товаров получить названия всех категорий, в которых представлены товары;*/
SELECT 
  c.`Category_Name` 
FROM
  `product_to_cat` pc 
  INNER JOIN `categories` c 
    ON c.`Category_ID` = pc.`Category_ID` 
WHERE pc.`Product_ID` IN (1000, 1001, 1002) ;

/*b. Для заданной категории получить список предложений всех товаров из этой категории и ее дочерних категорий; (2 запроса)*/
SELECT 
  ca.`Category_ID` 
FROM
  `categories` c,
  `categories` ca 
WHERE ca.`Category_Tree_Left_Key` BETWEEN c.`Category_Tree_Left_Key` 
	AND c.`Category_Tree_Right_Key` 
  AND c.`Category_ID` = 1013 ;

SELECT 
  p.Product_ID,
  p.`Product_Name`,
  p.`Product_Price` 
FROM
  product_to_cat pc 
  LEFT JOIN products p 
    ON pc.`Product_ID` = p.Product_ID 
WHERE pc.`Category_ID` IN (1013, 1019, 1020, 1021) 
GROUP BY pc.Product_ID ;	/*Групировка чтобы избавиться от дублей*/

/*b. Для заданной категории получить список предложений всех товаров из этой категории и ее дочерних категорий;(с вложенным запросом)*/
SELECT 
  p.Product_ID,
  p.`Product_Name`,
  p.`Product_Price` 
FROM
  product_to_cat pc 
  LEFT JOIN products p 
    ON pc.`Product_ID` = p.Product_ID 
WHERE pc.`Category_ID` IN 
  (SELECT 
    ca.Category_ID 
  FROM
    categories c,
    categories ca 
  WHERE ca.`Category_Tree_Left_Key` BETWEEN c.`Category_Tree_Left_Key` 
	AND c.`Category_Tree_Right_Key` 
    AND ca.`Category_Tree_Right_Key` BETWEEN c.`Category_Tree_Left_Key` 
	AND c.`Category_Tree_Right_Key` 
    AND c.`Category_ID` = 1013) 
GROUP BY pc.Product_ID ;	/*Групировка чтобы избавиться от дублей*/

		
/*c. Для заданного списка категорий получить количество предложений товаров в каждой категории;*/
SELECT 
  pc.`Category_ID`,
  COUNT(DISTINCT pc.`Product_ID`) AS cnt 
FROM
  `product_to_cat` pc 
WHERE pc.`Category_ID` IN (1013, 1019, 1020, 1021) 
GROUP BY pc.`Category_ID` ;

/*d. Для заданного списка категорий получить общее количество уникальных предложений товара;*/
SELECT 
  COUNT(DISTINCT pc.`Product_ID`) AS cnt 
FROM
  `product_to_cat` pc 
WHERE pc.`Category_ID` IN (1013, 1019, 1020, 1021);

/*e. Для заданной категории получить ее полный путь в дереве (breadcrumb, «хлебные крошки»).*/
SELECT 
  ca.`Category_ID`,
  ca.`Category_Name` 
FROM
  `categories` c,
  `categories` ca 
WHERE c.`Category_Tree_Left_Key` BETWEEN ca.`Category_Tree_Left_Key` 
	AND ca.`Category_Tree_Right_Key` 
  AND c.`Category_ID` = 1025 
  /*AND ca.`Category_ID` != 1025*/ /*Этим условием можно исключить из выборки ту категорию которую мы запрашиваем*/
GROUP BY ca.`Category_Parent_ID` 
ORDER BY ca.`Category_Parent_ID` ASC 