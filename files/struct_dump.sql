/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.6.31 : Database - test
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`test` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `test`;

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `Category_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Category_Parent_ID` int(11) unsigned NOT NULL DEFAULT '0',
  `Category_Level` int(11) unsigned NOT NULL DEFAULT '1',
  `Category_Tree_Left_Key` int(11) NOT NULL,
  `Category_Tree_Right_Key` int(11) NOT NULL,
  `Category_Alias` varchar(255) DEFAULT NULL,
  `Category_Name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Category_ID`),
  KEY `Category_Tree_Left_Key` (`Category_Tree_Left_Key`),
  KEY `Category_Tree_Right_Key` (`Category_Tree_Right_Key`)
) ENGINE=InnoDB AUTO_INCREMENT=1027 DEFAULT CHARSET=utf8;

/*Table structure for table `product_to_cat` */

DROP TABLE IF EXISTS `product_to_cat`;

CREATE TABLE `product_to_cat` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Product_ID` int(11) unsigned NOT NULL,
  `Category_ID` int(11) unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Product_ID` (`Product_ID`),
  KEY `Category_ID` (`Category_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8;

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `Product_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Product_Name` varchar(255) DEFAULT NULL,
  `Product_Price` double(10,2) unsigned DEFAULT NULL,
  PRIMARY KEY (`Product_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1151 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
