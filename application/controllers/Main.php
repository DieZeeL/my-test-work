<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }

    /**
    * Главная страница
    */
    public function index()
    {
        if(!is_login())
            redirect(base_url('?c=main&m=login'));
        // Доп задание №2: Гугл реально умный :)
        $code = highlight_file(__FILE__,true);
        $body = $this->load->view('index',array('code'=>$code),true);
        $this->load->view('body',array('user'=>$this->auth->user,'body'=>$body));    
    }

    /**
    * Страничка Логина
    */
    public function login(){
        if(is_login())
            redirect('/');       
        if($this->input->post()){
            if($this->auth->login($this->input->post('u',true),$this->input->post('p',true))){
                redirect(base_url());
            }
        }
        $this->load->view('login');    
    }

    /**
    * Ну соответственно выход :)
    */
    public function logout(){
        $this->auth->logout();
        redirect(base_url('?c=main&m=login'));    
    }

    /**
    * Покажем свою реализацию
    * библиотеки для БД на текстовом файле :)
    */
    public function db_lib(){
        if(!is_login())
            redirect(base_url('?c=main&m=login'));
        $code = highlight_file(APPPATH.DIRECTORY_SEPARATOR.'libraries'.DIRECTORY_SEPARATOR.'TextDB.php',true);   
        $body = $this->load->view('db_lib',array('code'=>$code),true);
        $this->load->view('body',array('user'=>$this->auth->user,'body'=>$body));    
    }
    /**
    * Покажем свою реализацию
    * библиотеки для Авторизации с логикой бана :)
    */
    public function auth_lib(){
        if(!is_login())
            redirect(base_url('?c=main&m=login'));
        $code = highlight_file(APPPATH.DIRECTORY_SEPARATOR.'libraries'.DIRECTORY_SEPARATOR.'Auth.php',true);   
        $body = $this->load->view('auth_lib',array('code'=>$code),true);
        $this->load->view('body',array('user'=>$this->auth->user,'body'=>$body));    
    }
    /**
    * Страничка со структурой БД
    */
    public function sql_struct(){
        if(!is_login())
            redirect(base_url('?c=main&m=login'));  
        $body = $this->load->view('sql_struct',array(),true);
        $this->load->view('body',array('user'=>$this->auth->user,'body'=>$body));    
    }
    /**
    * Страничка с запросами
    */
    public function queries(){
        if(!is_login())
            redirect(base_url('?c=main&m=login'));  
        $body = $this->load->view('queries',array(),true);
        $this->load->view('body',array('user'=>$this->auth->user,'body'=>$body));    
    }
    /**
    * Пример модели для Nested Sets
    */
    public function nested(){
        if(!is_login())
            redirect(base_url('?c=main&m=login'));  
        $body = $this->load->view('nested',array(),true);
        $code = highlight_file(APPPATH.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'Categories_model.php',true);   
        $body = $this->load->view('nested',array('code'=>$code),true);
        $this->load->view('body',array('user'=>$this->auth->user,'body'=>$body));    
    }
}
