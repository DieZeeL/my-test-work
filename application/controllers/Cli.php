<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cli extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function import_cat($file = false){
        if($file == false)
            $file = $this->input->get('file');
        if(!$file || !is_file(FCPATH.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.$file)){
            echo "Please input path to file!".PHP_EOL;
            return false;    
        }
        foreach(file(FCPATH.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.$file) as $line) {
            $a = str_getcsv($line,';');
            $a = array(
                'Category_ID' => $a[0],
                'Category_Parent_ID' => $a[2],
                'Category_Name' => $a[1],
                'Category_Level' => $a[3],
            );
            if($a['Category_ID'] != 'CatalogID')
                $this->db->insert('categories',$a);
            echo $line. "\n";
        }
    }
    
    public function cat(){
        $this->load->model('Categories_model','categories');
        $this->categories->indexTree();
    }
    
    public function import_prod($file = false){
        if($file == false)
            $file = $this->input->get('file');
        if(!$file || !is_file(FCPATH.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.$file)){
            echo "Please input path to file!".PHP_EOL;
            return false;    
        }
        foreach(file(FCPATH.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.$file) as $line) {
            $a = str_getcsv($line,';');
            $new = array(
                'Product_ID' => $a[0],
                'Product_Name' => $a[1],
                'Product_Price' => $a[2],
            );
            if($new['Product_ID'] != 'ID'){
                $this->db->insert('products',$new);
                $id = $this->db->insert_id();
                $this->db->insert('product_to_cat',array('Product_ID'=>$id,'Category_ID'=>$a[3]));
                if($a[4] != ""){
                    $this->db->insert('product_to_cat',array('Product_ID'=>$id,'Category_ID'=>$a[4]));    
                }
            
            }
            echo $line. "\n";
        }
    }
}
