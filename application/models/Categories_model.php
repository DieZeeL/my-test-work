<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Класс работы с категориями
*/
class Categories_model extends CI_Model {


    function __construct()
    {
        // вызываем конструктор модели
        parent::__construct();
    }

    private $categories_table = 'categories';
    private $counters_table = 'items_counters';

    /**
    *  Функция забирает список регионов по выбранному методу
    * 
    * @param string $method    Определяет метод выборки категорий (parent,level)
    * @param mixed $ident     Строка выборки 
    * @param array $order      Сортировка
    * @param int $limit        Ограничитель
    * @return array            Массив со списком категорий
    */
    function getCategories($method = null, $ident = null, $order = null, $limit = null) 
    {
        if ($method == null)
        return false;

        $this->db->select('c.*, cp.Category_Name as Category_Parent_Name')
        ->from($this->categories_table.' c')
        ->join($this->categories_table.' cp', 'cp.Category_ID =  c.Category_Parent_ID', 'left');
        switch ($method) {
            case "parent" :
                $this->db->where('c.Category_Parent_ID', $ident);
                break;
            case "level" :
                if ($ident == "max")
                {
                    $this->db->where('c.Category_Level = (SELECT MAX(Category_Level) FROM '.$this->db->dbprefix($this->categories_table).')', NULL, FALSE);
                }
                elseif ($ident == "min")
                {
                    $this->db->where('c.Category_Level = (SELECT MIN(Category_Level) FROM '.$this->db->dbprefix($this->categories_table).')', NULL, FALSE);
                }
                else
                {
                    $this->db->where('c.Category_Level', $ident);
                }
                break;
            case "all" :
                break;
            default :
                return false;
        }
        if ($order and is_array($order))
        {
            foreach($order as $okey=>$oval)
            {
                $this->db->order_by($okey, $oval);
            }
        }
        if ($limit)
            $this->db->limit($limit);
        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
        if (!$result)
            return false;
        return $result;
    }

    /**
    * Функция выводит список подкатегорий бесконечного уровня вложенности
    * 
    * @param int $id           ID категории родителя
    * @param int $show_parent  Показывать ли саму категорию родотеля (0,1) OPTIONAL default: 1
    * @return array            Список категорий в массиве
    */
    function getChilds($id=null, $show_parent=1) 
    {
        if ($id == null)
            return false;

        $this->db->select('ca.*')
        ->from($this->categories_table.' c, '.$this->categories_table.' ca')
        ->where('ca.Category_Tree_Left_Key BETWEEN c.Category_Tree_Left_Key AND c.Category_Tree_Right_Key')
        ->where('ca.Category_Tree_Right_Key BETWEEN  c.Category_Tree_Left_Key AND c.Category_Tree_Right_Key')
        ->where('c.Category_ID', $id);
        if($show_parent != 1)
            $this->db->where('ca.Category_ID !=', $id);

        $query = $this->db->get();
        $result = $query->result_array();

        $query->free_result();
        if (!$result)
            return array();
        return $result;
    }

    /**
    *  Функция выводит категорию с полями в ассоциативном массиве
    * 
    * @param string $method    Определяет метод выборки категорий (id,name,alias,parent_name)
    * @param mixed $ident     Строка выборки (string or int), если метод parent_name то array(parent,current)
    * @return array    Выводит категорию с полями в ассоциативном массиве
    */
    function getCategory($method = null, $ident = null) 
    {

        if ($method == null)
            return false;
        if(is_array($ident) AND $method != "parent_name")
        return false;

        $this->db->from($this->categories_table.' c');
        switch ($method) {
            case "id" :
                $this->db->where('c.Category_ID', $ident);
                break;
            case "name" :
                $this->db->like('c.Category_Name', $ident, 'none');
                break;
            case "alias" :
                $this->db->like('c.Category_Alias', $ident, 'none');
                break;
            case "parent_name" :
                $this->db->join($this->categories_table.' cp', 'c.Category_Parent_ID =  cp.Category_ID', 'left');
                $this->db->like('cp.Category_Name', $ident[1], 'none');
                $this->db->like('c.Category_Name', $ident[2], 'none');
                break;
            case "level" :
                if ($ident == "max")
                {
                    $this->db->where('c.Category_Level = (SELECT MAX(Category_Level) FROM '.$this->db->dbprefix($this->categories_table).')', NULL, FALSE);
                }
                elseif ($ident == "min")
                {
                    $this->db->where('c.Category_Level = (SELECT MIN(Category_Level) FROM '.$this->db->dbprefix($this->categories_table).')', NULL, FALSE);
                }
                else
                {
                    $this->db->where('c.Category_Level', $ident, 'none');
                }
                break;
            default :
                return false;
        }
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->row_array();
        $query->free_result();
        if (!$result)
            return false;
        return $result;
    }

    /**
    * Функция выводит список всех родителей категории
    * 
    * @param int $id           ID категории
    * @param int $show_current  Показывать ли саму категорию (0,1) OPTIONAL, default: 1 
    * @param string $order     Сортировка списка (asc,desc) OPTIONAL, default: asc
    * @return array            Список категорий в массиве
    */
    function getParents($id=null, $show_current=1, $order = 'asc') 
    {   
        if ($id == null)
            return false;
        $this->db->select('ca.*')
        ->from($this->categories_table.' c, '.$this->categories_table.' ca')
        ->where('c.Category_Tree_Left_Key BETWEEN ca.Category_Tree_Left_Key AND ca.Category_Tree_Right_Key')
        ->where('c.Category_Tree_Right_Key BETWEEN  ca.Category_Tree_Left_Key AND ca.Category_Tree_Right_Key')
        ->where('c.Category_ID', $id)
        ->group_by('ca.Category_Parent_ID')
        ->order_by('ca.Category_Parent_ID',$order);
        if($show_current != 1)
            $this->db->where('ca.Category_ID !=', $id);

        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
        if (!$result)
            return false;
        return $result;
    }

    /**
    * Переиндексирует дерево категорий по методу Джо Селко
    * 
    * @param int $parentId Родительская директория  
    * @param int $start    Внутрення переменная рекурсии
    * @return array 
    */  
    function indexTree($parentId = 0, $start = 1)
    {
        $query = $this->db->select('*')
        ->where('Category_Parent_ID', $parentId)
        ->get($this->categories_table);
        if ($query->num_rows() > 0)
        {
            $tree = $query->result_array();
            foreach ($tree as $el)
            {
                $this->db->where('Category_ID',$el['Category_ID'])
                ->update($this->categories_table,array('Category_Tree_Left_Key' => $start));
                list($start,$end)=$this->indexTree($el['Category_ID'], $start+1);
                $this->db->where('Category_ID',$el['Category_ID'])
                ->update($this->categories_table,array('Category_Tree_Right_Key' => $end));

            }
            return array($start+1,$start);
        }
        else
        {
            return array($start+1,$start);
        }

    }

    /**
    * Раскладывает категории в массив ввиде дерева
    * @param array $dataset Массив категорий
    * @param int $lvl Родитель
    * @return array
    */
    function getMapTree($dataset,$lvl) 
    {
        $tree = array(); // Создаем новый массив
        foreach ($dataset as $id=>&$node) {    
            if ($node['Category_Parent_ID'] == $lvl) { // не имеет родителя, т.е. корневой элемент
                $tree[$id] = &$node;
            } else { 
                /*
                Иначе это чей-то потомок
                этого потомка переносим в родительский элемент, 
                при этом у родителя внутри элемента создастся массив childs, в котором и будут вложены его потомки
                */
                $dataset[$node['Category_Parent_ID']]['childs'][$id] = &$node; //
            }
        }
        return $tree;
    }

    /**
    * Не смотреть, не дописана!
    * На стадии разработки! :)
    * 
    * @param mixed $data
    */
    function insertCat(array $data = array()){
        $parent = array();
        $max_key = 0;
        if($data['Category_Parent_ID'] == 0){
            $data['Category_Level'] = 1;
            $this->db->select_max('Category_Tree_Right_Key');
            $this->db->from($this->categories_table);
            $query = $this->db->get();
            $data['Category_Tree_Left_Key'] = $query['max'] + 1;
        } else {
            $parent = $this->getCategory('id',$data['Category_Parent_ID']);
            if(!$parent)
                return false;
            $data['Category_Level'] = $parent['Category_Level'] + 1;
            $data['Category_Tree_Left_Key'] = $parent['Category_Tree_Right_Key'];
        }
            $data['Category_Tree_Right_Key'] = $data['Category_Tree_Left_Key'] + 1;
    }
}
?>
