<?php defined('BASEPATH') OR exit('No direct script access allowed');


if ( ! function_exists('is_login'))
{

    function is_login()
    {
        return get_instance()->auth->isLogin();
    }

    function get_seconds_word_form($n){
        $forms=array('секунду', 'секунды', 'секунд');
        if ($n>0)
        {
            $n = abs($n) % 100;
            $n1 = $n % 10;
            if ($n > 10 && $n < 20) return $forms[2];
            if ($n1 > 1 && $n1 < 5) return $forms[1];
            if ($n1 == 1) return $forms[0];
        }
        return $forms[2];

    }
}