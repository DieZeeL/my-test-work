<html><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login Page</title>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/st.css" rel="stylesheet">
    <script type="text/javascript" src="/assets/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
    <body>
        <div class="login">
            <h1>Login</h1>
            <?php if($this->session->flashdata()) : ?>
                <?php foreach($this->session->flashdata() as $flash) : ?>
                <div class="alert alert-danger alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong><?=$flash;?></strong>
                </div>
                <?php endforeach;?>
                <?php endif; ?>

            <form method="post">
                <input type="text" name="u" placeholder="Имя" required="required">
                <input type="password" name="p" placeholder="Пароль" required="required">
                <button type="submit" class="btn btn-primary btn-block btn-large">Let me in.</button>
            </form>
        </div>

    </body>
</html>