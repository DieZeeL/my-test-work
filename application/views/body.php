<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Welcome to CodeIgniter</title>
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="/assets/css/st.css" rel="stylesheet">
        <link href="/assets/css/page.css" rel="stylesheet">
        <script type="text/javascript" src="/assets/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
    </head>
    <body>

        <div id="container">
            <div class="header">
                <h1>Добрый день, <?=ucfirst($user['login']);?>!</h1>
                <a href="<?=base_url('?c=main&m=logout');?>" type="button" class="btn btn-primary btn-inline btn-sm">Logout.</a>
            </div>
            <div id="body">
                <div id="menu">
                <ul class="nav nav-pills">
                    <li><a class="btn btn-primary btn-sm <?php if($this->router->fetch_method() == 'index') echo "active disabled";?>" href="<?=base_url();?>">Главная</a></li>
                    <li><a class="btn btn-primary btn-sm <?php if($this->router->fetch_method() == 'db_lib') echo "active disabled";?>" href="<?=base_url('?c=main&m=db_lib');?>">TextDB(Зад №1)</a></li>
                    <li><a class="btn btn-primary btn-sm <?php if($this->router->fetch_method() == 'auth_lib') echo "active disabled";?>" href="<?=base_url('?c=main&m=auth_lib');?>">Auth(Зад №1)</a></li>
                    <li><a class="btn btn-primary btn-sm <?php if($this->router->fetch_method() == 'sql_struct') echo "active disabled";?>" href="<?=base_url('?c=main&m=sql_struct');?>">Структура БД(Зад №2)</a></li>
                    <li><a class="btn btn-primary btn-sm <?php if($this->router->fetch_method() == 'queries') echo "active disabled";?>" href="<?=base_url('?c=main&m=queries');?>">Запросы к БД(Зад №2)</a></li>
                    <li><a class="btn btn-primary btn-sm <?php if($this->router->fetch_method() == 'nested') echo "active disabled";?>" href="<?=base_url('?c=main&m=nested');?>">Nested Sets(Бонус)</a></li>
                </ul>
                </div>
                <?=$body;?>
            </div>

            <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
        </div>

    </body>
</html>