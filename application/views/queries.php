<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<h4>Задание 2 </h4>
<p><a href="<?=base_url('files/queries.sql');?>" download>Скачать sql файл с запросами</a></p>
<p><strong>a.</strong> Для заданного списка товаров получить названия всех категорий, в которых представлены товары;</p>
<code>
<pre class="mysql" style="font-family:monospace;"><span style="color: #990099; font-weight: bold;">SELECT</span> 
  c.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Name`</span> 
<span style="color: #990099; font-weight: bold;">FROM</span>
  <span style="color: #008000;">`product<span style="color: #008080; font-weight: bold;">_</span>to<span style="color: #008080; font-weight: bold;">_</span>cat`</span> pc 
  <span style="color: #990099; font-weight: bold;">INNER</span> <span style="color: #990099; font-weight: bold;">JOIN</span> <span style="color: #008000;">`categories`</span> c 
    <span style="color: #990099; font-weight: bold;">ON</span> c.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>ID`</span> <span style="color: #CC0099;">=</span> pc.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>ID`</span>  
<span style="color: #990099; font-weight: bold;">WHERE</span> pc.<span style="color: #008000;">`Product<span style="color: #008080; font-weight: bold;">_</span>ID`</span> <span style="color: #990099; font-weight: bold;">IN</span> <span style="color: #FF00FF;">&#40;</span><span style="color: #008080;">1000</span><span style="color: #000033;">,</span> <span style="color: #008080;">1001</span><span style="color: #000033;">,</span> <span style="color: #008080;">1002</span><span style="color: #FF00FF;">&#41;</span><span style="color: #000033;">;</span></pre>
</code>

<p><strong>b.</strong> Для заданной категории получить список предложений всех товаров из этой категории и ее дочерних категорий;</p>
<code>
Здесь есть 2 пути реализации: Двумя отдельными запросами, что удобнее, либо одним, но со вложенным запросом.
<pre class="mysql" style="font-family:monospace;"><span style="color: #990099; font-weight: bold;">SELECT</span> 
  ca.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>ID`</span> 
<span style="color: #990099; font-weight: bold;">FROM</span>
  <span style="color: #008000;">`categories`</span> c<span style="color: #000033;">,</span>
  <span style="color: #008000;">`categories`</span> ca 
<span style="color: #990099; font-weight: bold;">WHERE</span> ca.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Tree<span style="color: #008080; font-weight: bold;">_</span>Left<span style="color: #008080; font-weight: bold;">_</span>Key`</span> <span style="color: #CC0099; font-weight: bold;">BETWEEN</span> c.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Tree<span style="color: #008080; font-weight: bold;">_</span>Left<span style="color: #008080; font-weight: bold;">_</span>Key`</span> 
  <span style="color: #CC0099; font-weight: bold;">AND</span> c.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Tree<span style="color: #008080; font-weight: bold;">_</span>Right<span style="color: #008080; font-weight: bold;">_</span>Key`</span> 
  <span style="color: #CC0099; font-weight: bold;">AND</span> c.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>ID`</span> <span style="color: #CC0099;">=</span> <span style="color: #008080;">1013</span> <span style="color: #000033;">;</span></pre>

 <pre class="mysql" style="font-family:monospace;"><span style="color: #990099; font-weight: bold;">SELECT</span> 
  p.Product_ID<span style="color: #000033;">,</span>
  p.<span style="color: #008000;">`Product<span style="color: #008080; font-weight: bold;">_</span>Name`</span><span style="color: #000033;">,</span>
  p.<span style="color: #008000;">`Product<span style="color: #008080; font-weight: bold;">_</span>Price`</span> 
<span style="color: #990099; font-weight: bold;">FROM</span>
  product_to_cat pc 
  <span style="color: #000099;">LEFT</span> <span style="color: #990099; font-weight: bold;">JOIN</span> products p 
    <span style="color: #990099; font-weight: bold;">ON</span> pc.<span style="color: #008000;">`Product<span style="color: #008080; font-weight: bold;">_</span>ID`</span> <span style="color: #CC0099;">=</span> p.Product_ID 
<span style="color: #990099; font-weight: bold;">WHERE</span> pc.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>ID`</span> <span style="color: #990099; font-weight: bold;">IN</span> <span style="color: #FF00FF;">&#40;</span><span style="color: #008080;">1013</span><span style="color: #000033;">,</span> <span style="color: #008080;">1019</span><span style="color: #000033;">,</span> <span style="color: #008080;">1020</span><span style="color: #000033;">,</span> <span style="color: #008080;">1021</span><span style="color: #FF00FF;">&#41;</span> 
<span style="color: #990099; font-weight: bold;">GROUP BY</span> pc.Product_ID <span style="color: #000033;">;</span>    <span style="color: #808000; font-style: italic;">/*Групировка чтобы избавиться от дублей*/</span></pre>

Вложенный запрос:

<pre class="mysql" style="font-family:monospace;"><span style="color: #990099; font-weight: bold;">SELECT</span> 
  p.Product_ID<span style="color: #000033;">,</span>
  p.<span style="color: #008000;">`Product<span style="color: #008080; font-weight: bold;">_</span>Name`</span><span style="color: #000033;">,</span>
  p.<span style="color: #008000;">`Product<span style="color: #008080; font-weight: bold;">_</span>Price`</span> 
<span style="color: #990099; font-weight: bold;">FROM</span>
  product_to_cat pc 
  <span style="color: #000099;">LEFT</span> <span style="color: #990099; font-weight: bold;">JOIN</span> products p 
    <span style="color: #990099; font-weight: bold;">ON</span> pc.<span style="color: #008000;">`Product<span style="color: #008080; font-weight: bold;">_</span>ID`</span> <span style="color: #CC0099;">=</span> p.Product_ID 
<span style="color: #990099; font-weight: bold;">WHERE</span> pc.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>ID`</span> <span style="color: #990099; font-weight: bold;">IN</span> 
  <span style="color: #FF00FF;">&#40;</span><span style="color: #990099; font-weight: bold;">SELECT</span> 
    ca.Category_ID 
  <span style="color: #990099; font-weight: bold;">FROM</span>
    categories c<span style="color: #000033;">,</span>
    categories ca 
  <span style="color: #990099; font-weight: bold;">WHERE</span> ca.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Tree<span style="color: #008080; font-weight: bold;">_</span>Left<span style="color: #008080; font-weight: bold;">_</span>Key`</span> <span style="color: #CC0099; font-weight: bold;">BETWEEN</span> c.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Tree<span style="color: #008080; font-weight: bold;">_</span>Left<span style="color: #008080; font-weight: bold;">_</span>Key`</span> 
    <span style="color: #CC0099; font-weight: bold;">AND</span> c.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Tree<span style="color: #008080; font-weight: bold;">_</span>Right<span style="color: #008080; font-weight: bold;">_</span>Key`</span> 
    <span style="color: #CC0099; font-weight: bold;">AND</span> ca.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Tree<span style="color: #008080; font-weight: bold;">_</span>Right<span style="color: #008080; font-weight: bold;">_</span>Key`</span> <span style="color: #CC0099; font-weight: bold;">BETWEEN</span> c.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Tree<span style="color: #008080; font-weight: bold;">_</span>Left<span style="color: #008080; font-weight: bold;">_</span>Key`</span> 
    <span style="color: #CC0099; font-weight: bold;">AND</span> c.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Tree<span style="color: #008080; font-weight: bold;">_</span>Right<span style="color: #008080; font-weight: bold;">_</span>Key`</span> 
    <span style="color: #CC0099; font-weight: bold;">AND</span> c.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>ID`</span> <span style="color: #CC0099;">=</span> <span style="color: #008080;">1013</span><span style="color: #FF00FF;">&#41;</span> 
<span style="color: #990099; font-weight: bold;">GROUP BY</span> pc.Product_ID<span style="color: #000033;">;</span>        <span style="color: #808000; font-style: italic;">/*Групировка чтобы избавиться от дублей*/</span> </pre>
</code>


<p><strong>c.</strong> Для заданного списка категорий получить количество предложений товаров в каждой категории;</p>
<code>
<pre class="mysql" style="font-family:monospace;"><span style="color: #990099; font-weight: bold;">SELECT</span> 
  pc.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>ID`</span><span style="color: #000033;">,</span>
  <span style="color: #000099;">COUNT</span><span style="color: #FF00FF;">&#40;</span><span style="color: #990099; font-weight: bold;">DISTINCT</span> pc.<span style="color: #008000;">`Product<span style="color: #008080; font-weight: bold;">_</span>ID`</span><span style="color: #FF00FF;">&#41;</span> <span style="color: #990099; font-weight: bold;">AS</span> cnt 
<span style="color: #990099; font-weight: bold;">FROM</span>
  <span style="color: #008000;">`product<span style="color: #008080; font-weight: bold;">_</span>to<span style="color: #008080; font-weight: bold;">_</span>cat`</span> pc 
<span style="color: #990099; font-weight: bold;">WHERE</span> pc.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>ID`</span> <span style="color: #990099; font-weight: bold;">IN</span> <span style="color: #FF00FF;">&#40;</span><span style="color: #008080;">1013</span><span style="color: #000033;">,</span> <span style="color: #008080;">1019</span><span style="color: #000033;">,</span> <span style="color: #008080;">1020</span><span style="color: #000033;">,</span> <span style="color: #008080;">1021</span><span style="color: #FF00FF;">&#41;</span> 
<span style="color: #990099; font-weight: bold;">GROUP BY</span> pc.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>ID`</span> <span style="color: #000033;">;</span></pre>
</code>


<p><strong>d.</strong> Для заданного списка категорий получить общее количество уникальных предложений товара;</p>
<code>
<pre class="mysql" style="font-family:monospace;"><span style="color: #990099; font-weight: bold;">SELECT</span> 
  <span style="color: #000099;">COUNT</span><span style="color: #FF00FF;">&#40;</span><span style="color: #990099; font-weight: bold;">DISTINCT</span> pc.<span style="color: #008000;">`Product<span style="color: #008080; font-weight: bold;">_</span>ID`</span><span style="color: #FF00FF;">&#41;</span> <span style="color: #990099; font-weight: bold;">AS</span> cnt 
<span style="color: #990099; font-weight: bold;">FROM</span>
  <span style="color: #008000;">`product<span style="color: #008080; font-weight: bold;">_</span>to<span style="color: #008080; font-weight: bold;">_</span>cat`</span> pc 
<span style="color: #990099; font-weight: bold;">WHERE</span> pc.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>ID`</span> <span style="color: #990099; font-weight: bold;">IN</span> <span style="color: #FF00FF;">&#40;</span><span style="color: #008080;">1013</span><span style="color: #000033;">,</span> <span style="color: #008080;">1019</span><span style="color: #000033;">,</span> <span style="color: #008080;">1020</span><span style="color: #000033;">,</span> <span style="color: #008080;">1021</span><span style="color: #FF00FF;">&#41;</span><span style="color: #000033;">;</span></pre>
</code>


<p><strong>e.</strong> Для заданной категории получить ее полный путь в дереве (breadcrumb, «хлебные крошки»).</p>
<code>
<pre class="mysql" style="font-family:monospace;"><span style="color: #990099; font-weight: bold;">SELECT</span> 
  ca.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>ID`</span><span style="color: #000033;">,</span>
  ca.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Name`</span> 
<span style="color: #990099; font-weight: bold;">FROM</span>
  <span style="color: #008000;">`categories`</span> c<span style="color: #000033;">,</span>
  <span style="color: #008000;">`categories`</span> ca 
<span style="color: #990099; font-weight: bold;">WHERE</span> c.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Tree<span style="color: #008080; font-weight: bold;">_</span>Left<span style="color: #008080; font-weight: bold;">_</span>Key`</span> <span style="color: #CC0099; font-weight: bold;">BETWEEN</span> ca.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Tree<span style="color: #008080; font-weight: bold;">_</span>Left<span style="color: #008080; font-weight: bold;">_</span>Key`</span> 
    <span style="color: #CC0099; font-weight: bold;">AND</span> ca.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Tree<span style="color: #008080; font-weight: bold;">_</span>Right<span style="color: #008080; font-weight: bold;">_</span>Key`</span> 
  <span style="color: #CC0099; font-weight: bold;">AND</span> c.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>ID`</span> <span style="color: #CC0099;">=</span> <span style="color: #008080;">1025</span> 
  <span style="color: #808000; font-style: italic;">/*AND ca.`Category_ID` != 1025*/</span> <span style="color: #808000; font-style: italic;">/*Этим условием можно исключить из выборки ту категорию которую мы запрашиваем*/</span>
<span style="color: #990099; font-weight: bold;">GROUP BY</span> ca.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Parent<span style="color: #008080; font-weight: bold;">_</span>ID`</span> 
<span style="color: #990099; font-weight: bold;">ORDER BY</span> ca.<span style="color: #008000;">`Category<span style="color: #008080; font-weight: bold;">_</span>Parent<span style="color: #008080; font-weight: bold;">_</span>ID`</span> <span style="color: #990099; font-weight: bold;">ASC</span> </pre>
</code>
