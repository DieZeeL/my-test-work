<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Типа авторизация
* @author DieZ
*/
class Auth {
    protected $CI;
    protected $login = false;
    public $user = null;

    function __construct(){
        $this->CI =& get_instance();
        if($this->user = $this->CI->session->userdata('user')){
            $this->login = true;  
        }
    }

    public function isLogin(){
        return $this->login;
    }
    /**
    * Впихнул сюда много чего, и проверку по ИП и по логину.
    * @param string $u      Логин
    * @param string $p      Пароль
    * @return boolean true|false
    */
    public function login($u,$p){
        $time = time();
        $ip = $this->CI->input->ip_address();
        // Проверяем, нет ли блокировок по IP
        if($this->CI->textdb->banned_ip->find($ip)){
            if($this->CI->textdb->banned_ip->{$ip}->blocked > $time){
                $s = $this->CI->textdb->banned_ip->{$ip}->blocked - $time;
                $this->CI->session->set_flashdata('banned','Попробуйте еще раз через '.$s.' '.get_seconds_word_form($s));
                return false;
            } elseif($this->CI->textdb->banned_ip->{$ip}->errors_count == 0) {
                // Чистим БД, возможно слегка избыточно, т.к. мы его потом можем снова добавить, но пусть будет так!    
                unset($this->CI->textdb->banned_ip->{$ip});    
            }

        }

        // Ищем юзера
        if($this->CI->textdb->users->find($u)){
            // Проверяем, не заблокирован ли он
            if($this->CI->textdb->users->{$u}->blocked > $time){
                $this->CI->session->set_flashdata('banned','Попробуйте еще раз через '.($this->CI->textdb->users->{$ip}->blocked - $time).' секунд');
                return false;    
            }
            // Проверяем пароль
            if(password_verify($p,$this->CI->textdb->users->{$u}->password)){
                $this->CI->session->set_userdata('user',$this->CI->textdb->users->get($u));
                $this->user = $this->CI->textdb->users->get($u);
                $this->login = true; 
                return true;
            } else {
                // Иначе или добавляем ошибок входа или блокируем, так же добавляем ошибок входя для его IP.
                $this->CI->textdb->users->{$u}->errors_count +=1;
                $this->CI->textdb->banned_ip->{$ip}->errors_count +=1;
                $this->CI->session->set_flashdata('banned','Неверные данные.');
                if($this->CI->textdb->users->{$u}->errors_count >= 3){
                    $this->CI->textdb->users->{$u}->blocked = $time + 300;
                    $this->CI->textdb->users->{$u}->errors_count = 0;
                    $s = $this->CI->textdb->banned_ip->{$ip}->blocked - $time;
                    $this->CI->session->set_flashdata('banned','Попробуйте еще раз через '.$s.' '.get_seconds_word_form($s));    
                }
            }
        } else {
            // Если не нашли юзера, проверяем, попадался ли уже нам этот IP, если нет, заносим в базу.
            if(!$this->CI->textdb->banned_ip->find($ip)){
                $this->CI->textdb->banned_ip->{$ip} = array('errors_count'=>0,'blocked'=>0);
            }
            // Добавляем ему ошибок.
            $this->CI->textdb->banned_ip->{$ip}->errors_count +=1;
            $this->CI->session->set_flashdata('banned','Неверные данные.');
            // Провыеряем, не пора ли заблокировать.
            if($this->CI->textdb->banned_ip->{$ip}->errors_count >= 3){
                $this->CI->textdb->banned_ip->{$ip}->blocked = $time + 300;
                $this->CI->textdb->banned_ip->{$ip}->errors_count = 0;
                $s = $this->CI->textdb->banned_ip->{$ip}->blocked - $time;
                $this->CI->session->set_flashdata('banned','Попробуйте еще раз через '.$s.' '.get_seconds_word_form($s));    
            }
        }
        return false;
    }

    public function logout(){
        if($this->login){
            $this->CI->session->unset_userdata('user');
            $this->login = false;
        }
        return true;    
    }
}

?>
