<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Класс работы с текстовой БД.
* @author DieZ
*/
class TextDB {
    
    private $filename = FCPATH.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'db.txt';
    private $destr_save = true;         // Если поставить в true, то мы уменьшим кол-во перезаписей файла, но возможно увеличим риск потери не
                                        // сохраненных данных, т.к. в этом случае будет происходить сохранение при завершении работы класса.
                                        // если false, то мы сохраняем файл при каждолм изменении данных.
    private $data = array();
    private $origin = array();

    
    /**
    * Конструируем нашу БД
    * если $data не указан, то читаем БД с файла,
    * если файла нет, то создаем.
    * 
    * @param mixed $data
    */
    function __construct(&$data = null, &$origin = null) {
        if($origin !== null)
            $this->origin = &$origin;
        else
            $this->origin = &$this->data;
        if($data !== null){
            $this->data = &$data;
        } else{
            
            if(!file_exists($this->filename)){
                $this->data = array(
                    'users' => [
                        'hamster' => [
                            'login' => 'hamster',
                            'password' => password_hash('MegaHamsterPass',PASSWORD_BCRYPT),
                            'errors_count' => 0,
                            'blocked' => 0,
                        ],
                    ],
                    'banned_ip'=> [
                        '127.0.0.1' => [
                            'errors_count' => 0,
                            'blocked'=>0,
                        ]
                    ]
                );
                $this->save();
            } else { 
                $handle = fopen($this->filename, 'r');
                $str = fread($handle,filesize($this->filename));
                $this->data = json_decode($str,true);
            }
        }
    }
    
    /**
    * Отработка при разрушении класса
    * Если $destr_save = true то здесь производим запись БД в файл
    */
    function __destruct(){
        if($this->destr_save === true)
            $this->save();    
    }

    /**
    * Вроде обычный геттер, но с приколом 
    * Если полученный параметр является массивом,
    * то мы делаем замыкание сами на себя - возвращаем себя же
    * для доступа к следующему уровню массива.
    * 
    * @param mixed $name
    * @return mixed TextDB|array
    */
    public function __get($name){
        if(!isset($this->data[$name]))
            $this->addParam($name);
        if(is_array($this->data[$name]))
            return new self($this->data[$name],$this->origin);
        else
            return $this->data[$name]; 
    }

    /**
    * Обычный сеттер
    * но дополнительно если $destr_save false
    * мы производим здесь запись в файл.
    * 
    * @param mixed $name
    * @param mixed $value
    */
    public function __set($name, $value ){
        $this->data[$name] = $value;
        if($this->destr_save === false)
            $this->save();
    }
    
    /**
    * Ничего интересного :)
    * 
    * @param mixed $name
    */
    public function __unset($name) 
    {
        unset($this->data[$name]);
    }

    /**
    * Соответственно функция записи в файл.
    * Тут стоит хитрая проверка чтобы не записывать каждый раз
    * файл когда класс вызван рекурсией
    * в массиве origin мы храним оригинал данных с БД
    */
    private function save() {
        if($this->data == $this->origin){
            $handle = fopen($this->filename,'w');
            fwrite($handle, json_encode($this->origin));
            fclose($handle);
        }    
    }

    /**
    * Можем забрать все данные
    */
    public function getData(){
        return $this->data;    
    }

    /**
    * Метод поиска по ключам
    * 
    * @param mixed $name
    */
    public function find($name){
        if(is_array($this->data))
            return array_key_exists($name,$this->data);
        return false;
    }

    /**
    * Небольшой доп метод для получения какого-то параметра
    * из БД если он является массивом, т.к. геттер в случае 
    * массива вернет не сам массив а созданный на основе него объект.
    * 
    * @param mixed $name
    */
    public function get($name){
        return $this->data[$name];
    } 
    
    public function addParam($name){
        if(!isset($this->data[$name])){
            $this->data[$name] = array();
        }
    } 

}
?>
